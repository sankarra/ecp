import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

import {User} from './user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private users: User[] = [
    new User('arun', '123', 'Arunkumar', 'Sankarram', 'test@gmail.com', ['Bank']),
    new User('sarav', '123', 'Sarav', 'Subramanian', 'test@gmail.com', ['Admin']),
    new User('rick', '123', 'Rick', 'Kowal', 'test@gmail.com', ['BillPay']),
    new User('timd', '123', 'Tim', 'Dewey', 'test@gmail.com', ['Admin']),
    new User('suresh', '123', 'Suresh', 'Rajan', 'test@gmail.com', ['Admin']),
    new User('timf', '123', 'Tim', 'Freitag', 'test@gmail.com', ['Admin']),
    new User('arthur', '123', 'Arthur', 'Miller', 'test@gmail.com', ['Admin']),
    new User('wayne', '123', 'Wayne', 'Parkhurst', 'test@gmail.com', ['Admin']),
    new User('rahul', '123', 'Rahul', 'Gaddam', 'test@gmail.com', ['Admin']),
    new User('ryo', '123', 'Ryo', 'Koyama', 'test@gmail.com', ['Admin']),
    new User('carol', '123', 'Carol', 'Paige', 'test@gmail.com', ['Admin']),
  ];

  private messageSource = new BehaviorSubject(null);
  loggedInUser = this.messageSource.asObservable();

  login(userName: string, password: string) {
    const currentUser = this.users.filter((user) => user.userName === userName.toLocaleLowerCase() && user.password === password);
    this.messageSource.next(currentUser);

    console.log(currentUser);
    return currentUser;
  }

  logout() {
    this.messageSource.next(null);
  }
}
