import {last} from 'rxjs/operators';

export class User {
  public userName: string;
  public password: string;
  public firstName: string;
  public lastName: string;
  public email: string;
  public roles: string[];

  constructor(userName: string, password: string, firstName: string, lastName: string, email: string, roles: string[]) {
    this.userName = userName,
      this.password = password,
      this.firstName = firstName,
      this.lastName = lastName,
      this.email = email,
      this.roles = roles;
  }
}
