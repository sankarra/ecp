import {Component, OnInit} from '@angular/core';
import {LoginService} from '../user/login.service';
import {User} from '../user/user.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  userObj: User;
  filterdApps: object[] = [];

  roleAndApps = [{
    appName: 'Developers',
    iconPath: '../../assets/images/Land_Developers.jpg',
    roles: ['Admin', 'Dev']
  },
    {
      appName: 'UI/UX Certification',
      iconPath: '../../assets/images/Land_UIUXCert.png',
      roles: ['Admin']
    },
    {
      appName: 'Organization Administration',
      iconPath: '../../assets/images/Land_OrgAdministration.png',
      roles: ['Admin']
    },
    {
      appName: 'Contributed Data',
      iconPath: '../../assets/images/Land_ContributedData.png',
      roles: ['Admin', 'Bank']
    },
    {
      appName: 'Third Party Sender',
      iconPath: '../../assets/images/Land_ThirdPartySender.png',
      roles: ['Admin', 'Bank', 'BillPay']
    },
    {
      appName: 'Bill Pay',
      iconPath: '../../assets/images/Land_BillPay.gif',
      roles: ['Admin', 'Bank', 'BillPay']
    },
    {
      appName: 'Account Owner Auth',
      iconPath: '../../assets/images/Land_AccountOwnerAuthent.png',
      roles: ['Admin', 'Bank']
    },
    {
      appName: 'Complaince',
      iconPath: '../../assets/images/Land_Complaince.png',
      roles: ['Admin']
    },
    {
      appName: 'Money Movement',
      iconPath: '../../assets/images/Land_MoneyMovement.png',
      roles: ['Admin', 'Bank', 'BillPay']
    },
    {
      appName: 'Zelle Onboarding',
      iconPath: '../../assets/images/Land_ZelleOnBoarding.png',
      roles: ['Admin']
    },
    {
      appName: 'Fraud Research',
      iconPath: '../../assets/images/Land_FraudResearch.png',
      roles: ['Admin', 'Bank']
    },
    {
      appName: 'Disputes',
      iconPath: '../../assets/images/Land_Disputes.jpg',
      roles: ['Admin', 'Bank', 'BillPay']
    },
  ];

  constructor(private user: LoginService, public router: Router) {
  }

  ngOnInit() {
    this.user.loggedInUser.subscribe(usr => this.userObj = usr);
    if (this.userObj != null) {
      this.filterdApps.push(this.roleAndApps.filter(item => item.roles.indexOf(this.userObj[0].roles.toString()) > -1));
    }

    console.log(this.filterdApps);
  }


}
