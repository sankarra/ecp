import {Component, OnInit} from '@angular/core';
import {LoginService} from '../user/login.service';
import {User} from '../user/user.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userObj: User;
  title = 'Early Warning Enterprise Portal';

  constructor(private user: LoginService, public router: Router) {
  }

  ngOnInit() {
    this.user.loggedInUser.subscribe(usr => this.userObj = usr);
  }

  logout() {
    this.user.logout();
    this.router.navigate(['/login']);
  }

}
